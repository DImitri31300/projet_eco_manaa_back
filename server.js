// SET UP OF PAQUETS NPM
const session    = require('express-session');
const bodyParser = require('body-parser');
const express    = require('express');
const mysql      = require('mysql');

// CONFIG EXPRESS
const app        = express();

// CONFIG EXPRESS SESSION
app.use(session({
	'secret': '343ji43j4n3jn4jk3n'
  }))

// CONFIG BODY-PARSER
app.use(bodyParser.urlencoded({ extended: false })) 
app.use(bodyParser.json());

// SET UP OF DOTENV, il permet de charger les variables d'environnement à partir d'un ficher .env
require('dotenv').config();

// CONFIG AND CON BDD
const connection = mysql.createConnection({
	host : process.env.DB_HOST,
	user : process.env.DB_USER,
	password :"",
	database : process.env.DB_DATABASE 
});
connection.connect((err) => {
	if(err){
		console.log(err);
		return;
	}
	console.log('Vous avez accès à votre base de donnée.');
});

// CONFIG PUG
app.set('view engine', 'pug');




// SET UP ROAD
app.use(express.static(`${__dirname}/static`))

// ROAD RACINE
app.get('/', (req, res) => {
	res.render('challenge/index');
});
//ROAD TEST EXPRESS SESSION
app.get('/session', (req, res, next) => {
	req.session.name = "Flavio"
	console.log(req.session.name)
  });

// ROAD FORM REGISTER
app.get('/register', (req, res) => {
			res.render('challenge/register');
});

// INFORMATION RETRIVIAL OF users form regsiter
app.post('/challenge/form', (req, res) => {
	// let cookies = req.session.name = req.body.login
	// console.log(cookies)
	connection.query(`INSERT INTO users (login, mail, password) VALUES ('${req.body.login}', '${req.body.mail}', '${req.body.password}');`, (err, result, fields) => {
			if(err){
				console.log(err)
			}
	});
	res.redirect('/login');
});

// ROAD LOGIN
app.get('/login', (req, res) => {
			res.render('challenge/login')

});
// CONFIRMATION LOG
app.post('/login', (req, res,) => {
	let login          =  req.body.login_co;
	let password       =  req.body.password_co;
	let cookies_log    =  req.session.name = login
		console.log(cookies_log)
	connection.query(`SELECT * FROM users WHERE login = '${login}';`, (err, result, fields) => {
	if (result.length < 1) {
		res.redirect('/login');
	} else if (login === result[0].login) {
		if( password === result[0].password) { // COMPRENDRE ICI PK ON PASSE LE RESULT A [0] ?
			req.session.id_users = result[0].id
			res.redirect('/challenge')
		} else {
			res.redirect('/login');
		}
	}
	});
});

// ROAD CHALLENGE
app.get('/challenge', (req, res) => {
	connection.query(`SELECT * FROM challenges;`, (err, result, fields) => {
		if(err){
			console.log(err)
			return
		}
			res.render('challenge/challenge', {results: result, hope:req.session.name, id:req.session.id_users})
	});
});

// ROAD DEFI
app.get('/defi/:id', (req, res) => {
  req.params.id;
	connection.query(`SELECT * FROM challenges where id = '${req.params.id}';`, (err, result, fields) => { // Bien comprendre REQ.PARAMS.ID ?
		if(err){
			console.log(err);
			return
		}
		res.render('challenge/defi',{result: result[0],name:req.session.name })
	});
});
// ROAD DEFI ADVICE objectif récuprer la valeur du texteareea et céer une insertion dans la base de donnée 
// (Garder à l'esprit que l'id de l'users du commentaire doit étre lié ainsi que l'id du challenge) 
app.post('/defi/:id', (req, res) => {
	console.log(req.body.content)
	console.log(join);
	connection.query(`INSERT INTO advices (content, challenges_id, users_id) VALUES ('${req.body.advice}', '${req.params.id}','${req.session.id_users}');`, (err, result, fields) => {
			if(err){
				console.log(err)
			}
			test = req.params.id
			// res.render('challenge/defi',{result: result,hope:cookies_id })
			res.redirect(`/defi/${test}`);
	}); 
});

// ROAD PROFIL
app.get('/admin', (req, res) => {

});




// LOG SERVER 
app.listen(process.env.PORT, () => console.log(`Vous vous êtes correctement arrimé au port ${process.env.PORT}`));